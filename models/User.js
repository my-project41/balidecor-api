const { Model } = require('objection');
const knex = require('../db/connection');

Model.knex(knex);

class User extends Model {
  static get tableName() {
    return 'users';
  }

  static get relationMappings() {
    const UserProfile = require('./UserProfile');
    return {
      users_profiles: {
        relation: Model.HasManyRelation,
        modelClass: UserProfile,
        join: {
          from: 'users.id',
          to: 'users_profiles.user_id',
        },
      },
    };
  }
}

module.exports = User;
