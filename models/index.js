const User = require('./User');
const UserProfile = require('./UserProfile');

module.exports = {
  User,
  UserProfile,
};
