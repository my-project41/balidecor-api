const { Model } = require('objection');
const knex = require('../db/connection');

Model.knex(knex);

class UserProfile extends Model {
  static get tableName() {
    return 'users_profiles';
  }

  static get relationMappings() {
    const User = require('./User');
    return {
      writer: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'users_profiles.user_id',
          to: 'users.id',
        },
      },
    };
  }
}

module.exports = UserProfile;
