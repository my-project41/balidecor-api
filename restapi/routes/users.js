const express = require('express');
const { userController } = require('../controllers');

const router = express.Router();

router.get('/', (req, res) => {
  userController.getAll(req, res);
});

router.get('/:id', (req, res) => {
  userController.getOne(req, res);
});

module.exports = router;
