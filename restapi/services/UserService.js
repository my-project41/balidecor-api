const { User } = require('../../models');

module.exports = class UserService {
  constructor() {
    this.user = User;
  }

  async getAll(callback) {
    try {
      const result = await this.user.query();
      callback(null, {
        data: result,
      });
    } catch (error) {
      throw Error(error.message);
    }
  }

  async getOne(id, callback) {
    try {
      const result = await this.user
        .query()
        .where('id', id)
        .eager('users_profiles');
      callback(null, result);
    } catch (error) {
      throw Error(error.message);
    }
  }
};
