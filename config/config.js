module.exports = {
  app: {
    port: 6996,
    host: process.env.HOST || 'localhost',
    mainRoute: '/balidecor/api/v1',
    modeServer: 'http',
    modeCluster: true,
    modeRedis: true,
    openSslKeyPath: './key.pem',
    openSslCertPath: './cert.pem',
    loggerFilePath: './logs/access.log',
    rateLimitSuspendTime: 5,
    rateLimitMaxHitPerIP: 500,
    pageLimit: 10,
  },
};
