# BALIDECOR-API

## What's included

- `mysql` - MySQL module
- `knex` - Query builder
- `objection` - ORM built upon Knex
- `express` - API framework

## How to use it

- Fork/Clone
- `cd balidecor-api`
- `npm install` to install required packages
- Add a *.env* file
- Create local mysql databases - `balidecor`
- Modify database connection according to your machine in `knexfile.js`
- Migrate - `knex migrate:latest --env development`
- Seed - `knex seed:run --env development`
- `npm start` to start the server
- Point to `http://localhost:6996/balidecor/api/v1/users` or `http://localhost:6996/balidecor/api/v1/users/1`  to test if it's working
