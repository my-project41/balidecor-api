const config = require('../config/config');
const { apiLimiter } = require('../main/apiLimiter');

module.exports = function(app) {
  app.use(config.app.mainRoute + '/users', apiLimiter);
  app.use(config.app.mainRoute + '/users', require('../restapi/routes/users'));
};
