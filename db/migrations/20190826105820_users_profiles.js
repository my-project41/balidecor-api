exports.up = async function(knex, Promise) {
  await knex.schema.createTable('users_profiles', table => {
    table
      .increments('id')
      .unsigned()
      .primary();
    table.string('content');
    table
      .integer('user_id')
      .references('id')
      .inTable('users')
      .unsigned();
    table.timestamps();
  });
};

exports.down = async function(knex, Promise) {
  await knex.schema.dropTable('users_profiles');
};
