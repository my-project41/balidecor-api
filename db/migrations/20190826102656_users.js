exports.up = async function(knex, Promise) {
  await knex.schema.createTable('users', table => {
    table
      .increments('id')
      .unsigned()
      .primary();
    table
      .string('email')
      .unique()
      .notNullable();
    table.string('username').notNullable();
    table.timestamps();
  });
};

exports.down = async function(knex, Promise) {
  await knex.schema.dropTable('users');
};
