exports.seed = async function(knex, Promise) {
  await knex('users').del();
  await knex('users').insert([
    { username: 'peter', email: 'mock1@email.com' },
    { username: 'john', email: 'mock2@email.com' },
    { username: 'david', email: 'mock3@email.com' },
  ]);
};
