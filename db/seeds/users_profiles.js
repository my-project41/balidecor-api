exports.seed = async function(knex, Promise) {
  await knex('users_profiles').del();
  await knex('users_profiles').insert([
    { content: 'Hello there!', user_id: 1 },
    { content: 'How are you', user_id: 2 },
    { content: 'You are very beautifull', user_id: 3 },
  ]);
};
